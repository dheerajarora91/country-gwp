﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountryGWP.Models
{
    public class CountryGwpRequest
    {
        public string Country { get; set; }
        public IEnumerable<string> Lob { get; set; }
    }
}
