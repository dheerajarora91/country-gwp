﻿using CountryGWP.Business.Layer.Services.Interfaces;
using CountryGWP.DataAccess.Layer.Entities;
using CountryGWP.DataAccess.Layer.Repositories.Interfaces;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace CountryGWP.UnitTest.ServiceTests
{
    public class CountryGwpServiceTest
    {
        private readonly ICountryGwpService _countryGwpService;
        private readonly ICountryGwpRepository _countryGwpRepository;

        public CountryGwpServiceTest()
        {
            _countryGwpRepository = Substitute.For<ICountryGwpRepository>();
               
            _countryGwpService = Substitute.For<ICountryGwpService>();
        }

        
    }
}
