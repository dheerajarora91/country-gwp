﻿using CountryGWP.DataAccess.Layer.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CountryGWP.DataAccess.Layer.Context
{
    public class CountryGwpDbContext : DbContext
    {
        public DbSet<CountryGwp> CountriesGwp { get; set; }

        public CountryGwpDbContext(DbContextOptions<CountryGwpDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            var countriesGwp = new List<CountryGwp>()
            {
                new CountryGwp()
                {
                    CountryGwpId = 1,
                    Country = "ae",
                    Lob = "transport",
                    Y2008 = 20.7,
                    Y2009 = 20.7,
                    Y2010 = 20.7,
                    Y2011 = 20.7,
                    Y2012 = 20.7,
                    Y2013 = 20.7,
                    Y2014 = 20.7,
                    Y2015 = 20.7,
                },
                new CountryGwp()
                {
                    CountryGwpId = 2,
                    Country = "ae",
                    Lob = "property",
                    Y2008 = 20.7,
                    Y2009 = 20.7,
                    Y2010 = 20.7,
                    Y2011 = 20.7,
                    Y2012 = 20.7,
                    Y2013 = 20.7,
                    Y2014 = 20.7,
                    Y2015 = 20.7,
                }
            };


            //var countriesGwp = File.Read("D:\\Self Study\\Country GWP\\CountryGWP\\CountryGWP.DataAccess.Layer\\Data\\gwpByCountry.csv")
            //                 .Skip(1)
            //                 .Select(v => FromCsv(v))
            //                 .ToList();

            foreach(var countryGwp in countriesGwp)
            {
                modelBuilder.Entity<CountryGwp>().HasData(
                    new CountryGwp
                    {
                        CountryGwpId = countryGwp.CountryGwpId,
                        Country = countryGwp.Country,
                        Lob = countryGwp.Lob,
                        Y2008 = countryGwp.Y2008,
                        Y2009 = countryGwp.Y2009,
                        Y2010 = countryGwp.Y2010,
                        Y2011 = countryGwp.Y2011,
                        Y2012 = countryGwp.Y2012,
                        Y2013 = countryGwp.Y2013,
                        Y2014 = countryGwp.Y2014,
                        Y2015 = countryGwp.Y2015,
                    });
            }
        }

        public static CountryGwp FromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            
            CountryGwp countryGwp = new CountryGwp();
            
            countryGwp.Country = values[0];
            countryGwp.Lob = values[3];
            countryGwp.Y2008 = Convert.ToDouble(values[12]);
            countryGwp.Y2009 = Convert.ToDouble(values[13]);
            countryGwp.Y2010 = Convert.ToDouble(values[14]);
            countryGwp.Y2011 = Convert.ToDouble(values[15]);
            countryGwp.Y2012 = Convert.ToDouble(values[16]);
            countryGwp.Y2013 = Convert.ToDouble(values[17]);
            countryGwp.Y2014 = Convert.ToDouble(values[18]);
            countryGwp.Y2015 = Convert.ToDouble(values[19]);

            return countryGwp;
        }
    }
}
