﻿// <auto-generated />
using CountryGWP.DataAccess.Layer.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace CountryGWP.DataAccess.Layer.Migrations
{
    [DbContext(typeof(CountryGwpDbContext))]
    [Migration("20200912094237_Initial-Migration")]
    partial class InitialMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.8")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("CountryGWP.DataAccess.Layer.Entities.CountryGwp", b =>
                {
                    b.Property<int>("CountryGwpId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Country")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Lob")
                        .HasColumnType("nvarchar(max)");

                    b.Property<double>("Y2008")
                        .HasColumnType("float");

                    b.Property<double>("Y2009")
                        .HasColumnType("float");

                    b.Property<double>("Y2010")
                        .HasColumnType("float");

                    b.Property<double>("Y2011")
                        .HasColumnType("float");

                    b.Property<double>("Y2012")
                        .HasColumnType("float");

                    b.Property<double>("Y2013")
                        .HasColumnType("float");

                    b.Property<double>("Y2014")
                        .HasColumnType("float");

                    b.Property<double>("Y2015")
                        .HasColumnType("float");

                    b.HasKey("CountryGwpId");

                    b.ToTable("CountriesGwp");

                    b.HasData(
                        new
                        {
                            CountryGwpId = 1,
                            Country = "ae",
                            Lob = "transport",
                            Y2008 = 20.699999999999999,
                            Y2009 = 20.699999999999999,
                            Y2010 = 20.699999999999999,
                            Y2011 = 20.699999999999999,
                            Y2012 = 20.699999999999999,
                            Y2013 = 20.699999999999999,
                            Y2014 = 20.699999999999999,
                            Y2015 = 20.699999999999999
                        },
                        new
                        {
                            CountryGwpId = 2,
                            Country = "ae",
                            Lob = "property",
                            Y2008 = 20.699999999999999,
                            Y2009 = 20.699999999999999,
                            Y2010 = 20.699999999999999,
                            Y2011 = 20.699999999999999,
                            Y2012 = 20.699999999999999,
                            Y2013 = 20.699999999999999,
                            Y2014 = 20.699999999999999,
                            Y2015 = 20.699999999999999
                        });
                });
#pragma warning restore 612, 618
        }
    }
}
