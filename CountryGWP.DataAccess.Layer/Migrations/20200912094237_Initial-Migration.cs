﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CountryGWP.DataAccess.Layer.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CountriesGwp",
                columns: table => new
                {
                    CountryGwpId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Country = table.Column<string>(nullable: true),
                    Lob = table.Column<string>(nullable: true),
                    Y2008 = table.Column<double>(nullable: false),
                    Y2009 = table.Column<double>(nullable: false),
                    Y2010 = table.Column<double>(nullable: false),
                    Y2011 = table.Column<double>(nullable: false),
                    Y2012 = table.Column<double>(nullable: false),
                    Y2013 = table.Column<double>(nullable: false),
                    Y2014 = table.Column<double>(nullable: false),
                    Y2015 = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CountriesGwp", x => x.CountryGwpId);
                });

            migrationBuilder.InsertData(
                table: "CountriesGwp",
                columns: new[] { "CountryGwpId", "Country", "Lob", "Y2008", "Y2009", "Y2010", "Y2011", "Y2012", "Y2013", "Y2014", "Y2015" },
                values: new object[] { 1, "ae", "transport", 20.699999999999999, 20.699999999999999, 20.699999999999999, 20.699999999999999, 20.699999999999999, 20.699999999999999, 20.699999999999999, 20.699999999999999 });

            migrationBuilder.InsertData(
                table: "CountriesGwp",
                columns: new[] { "CountryGwpId", "Country", "Lob", "Y2008", "Y2009", "Y2010", "Y2011", "Y2012", "Y2013", "Y2014", "Y2015" },
                values: new object[] { 2, "ae", "property", 20.699999999999999, 20.699999999999999, 20.699999999999999, 20.699999999999999, 20.699999999999999, 20.699999999999999, 20.699999999999999, 20.699999999999999 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CountriesGwp");
        }
    }
}
